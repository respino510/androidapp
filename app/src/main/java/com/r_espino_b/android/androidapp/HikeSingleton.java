package com.r_espino_b.android.androidapp;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Environment;

import com.r_espino_b.android.androidapp.database.HikeCursorWrapper;
import com.r_espino_b.android.androidapp.database.HikeDBSchema.HikeTable;
import com.r_espino_b.android.androidapp.database.HikeOpenHelper;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/* HikeSingleton: used as the middleman between
*  the Hike object's data and its storage. It's
*  services can only be accessed through s single
*  class object instance.
*/
public class HikeSingleton {
    private static HikeSingleton sHikeSingleton;    //ensures only one instance will exist

    private Context mContext;
    private SQLiteDatabase mDatabase;

    public static HikeSingleton get(Context context) {
        if(sHikeSingleton == null) {
            sHikeSingleton = new HikeSingleton(context);
        }
        return sHikeSingleton;
    }

    private HikeSingleton(Context context) {
        mContext = context.getApplicationContext();
        mDatabase = new HikeOpenHelper(mContext)
                .getWritableDatabase();
    }

    /* getHikes() creates an ArrayList to store hikes
    *  retrieved form the database table and returns
    *  the ArrayList.
    */
    public List<Hike> getHikes() {
        //hikes ArrayList created to store hikes
        //retrieved from the database table
        List<Hike> hikes = new ArrayList<>();

        //iterate database table and retrieve each hike and
        //store it in the hikes ArrayList one by one
        HikeCursorWrapper cursor = queryHikes(null, null);

        try {
            //iterate the database and return
            cursor.moveToFirst();
            while(!cursor.isAfterLast()) {
                hikes.add(cursor.getHike());
                cursor.moveToNext();
            }
        }
        finally {
            cursor.close();
        }

        return hikes;
    }

    /* getHike() is used to retrieve a single Hike
    *  object from the Hike Log using its unique
    *  id- UUID (mHikeID).
    */
    public Hike getHike(UUID hikeID) {
        HikeCursorWrapper cursor = queryHikes(
                HikeTable.COLUMNS.UUID + " = ?",
                new String[] {hikeID.toString()});

        try {
            //if table is empty, return null
            if(cursor.getCount() == 0) {
                return null;
            }
            /* if table not empty,
            /* return the first row (hike object)
            /* and its data values in table
            */
            cursor.moveToFirst();
            return cursor.getHike();
        }
        finally {
            cursor.close();
        }
    }

    /* This method creates a new file in the internal
    *  directory where the photos will be saved.
    */
    public File getInPhotoFile(Hike hike) {
        //returns a File (inFileDir) representing an internal directory
        File inFileDir = mContext.getFilesDir();
        //creates a new file in the inFileDir directory and passes the file name
        return new File(inFileDir, hike.getPhotoFilename());
    }

    /* updateHike() can be used to correct or change any
    *  information that is stored about a particular hike
    *  object in the hike log.
    */
    public void updateHike(Hike hike) {
        String uuidString = hike.getHikeID().toString();
        ContentValues values = getContentValues(hike);

        mDatabase.update(HikeTable.NAME, values,
                HikeTable.COLUMNS.UUID + " = ?",
                new String[] {uuidString});
    }

    // queryHikes() is used to traverse the hikes in the database table
    private HikeCursorWrapper queryHikes(String whereClause, String[] whereArgs) {
        Cursor cursor = mDatabase.query(
                HikeTable.NAME,
                null,
                whereClause,
                whereArgs,
                null,
                null,
                null
        );
        return new HikeCursorWrapper(cursor);
    }

    /* addHike(Hike h) takes in as a parameter a Hike
    *  object, h, created in HikeFragment and passes
    *  it to getContentValues(). The values returned
    *  from getContentValues() are inserted into the
    *  database table.
    */
    public void addHike(Hike hike) {
        //pass the hike object to insert into table
        ContentValues values = getContentValues(hike);

        //insert the hike object (row) and its correspond values (key/value pairs)
        mDatabase.insert(HikeTable.NAME, null, values);
    }

    /* getContentValues(Hike h) takes in as a parameter a Hike object,
    *  h, and extracts each of its data fields and associates them with
    *  their corresponding column in the database table. The key/value
    *  pairs are then returned as a set of key/value pairs.
    */
    private static ContentValues getContentValues(Hike hike) {
        ContentValues values = new ContentValues();

        /* sets key/value pairs, where the keys are the columns
        *  (attribute) of the database table and the values are
        *  the Hike's data fields state.
        */
        values.put(HikeTable.COLUMNS.UUID, hike.getHikeID().toString());
        values.put(HikeTable.COLUMNS.DATE, hike.getHikeDate().getTime());
        values.put(HikeTable.COLUMNS.TITLE, hike.getHikeName());
        values.put(HikeTable.COLUMNS.TIME, hike.getHikeTime());
        values.put(HikeTable.COLUMNS.DISTANCE, hike.getHikeDistance());
        values.put(HikeTable.COLUMNS.ALTITUDE, hike.getHikeAltitude());

        //returns the set of key/value pairs
        return values;
    }
}