package com.r_espino_b.android.androidapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

public class HikeLogFragment extends Fragment {
    private RecyclerView mHikeRecyclerView;
    private HikeAdapter mAdapter;

    private TextView mHikeNameTV;
    private TextView mHikeDateTV;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_hike_log, container, false);

        mHikeRecyclerView = (RecyclerView)v.findViewById(R.id.hike_recycler_view);
        mHikeRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        updateUI();

        return v;
    }

    private void updateUI() {
        HikeSingleton hikeSingleton = HikeSingleton.get(getActivity());
        List<Hike> hikes = hikeSingleton.getHikes();

        if(mAdapter == null) {
            mAdapter = new HikeAdapter(hikes);
            mHikeRecyclerView.setAdapter(mAdapter);
        }
        else {
            mAdapter.setHikes(hikes);
            mAdapter.notifyDataSetChanged();
        }
    }

    private class HikeViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private Hike mHike;

        public HikeViewHolder(LayoutInflater inflater, ViewGroup parent) {
            super(inflater.inflate(R.layout.individual_log_hike, parent, false));

            mHikeNameTV = (TextView)itemView.findViewById(R.id.hike_name_tv);
            mHikeDateTV = (TextView)itemView.findViewById(R.id.hike_date_tv);
        }

        public void bind(Hike hike) {
            mHike = hike;
            mHikeNameTV.setText(mHike.getHikeName());
            mHikeDateTV.setText(mHike.getHikeDate().toString());
        }

        @Override
        public void onClick(View v) {
            Intent intent = HikeActivity.newIntent(getActivity(), mHike.getHikeID());
            startActivity(intent);
        }
    }

    private class HikeAdapter extends RecyclerView.Adapter<HikeViewHolder> {
        private List<Hike> mHikes;

        public HikeAdapter(List<Hike> hikes) {
            mHikes = hikes;
        }

        @Override
        public HikeViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater layoutInflater = LayoutInflater.from(getActivity());

            return new HikeViewHolder(layoutInflater, parent);
        }

        @Override
        public void onBindViewHolder(HikeViewHolder holder, int position) {
            Hike hike = mHikes.get(position);
            holder.bind(hike);
        }

        @Override
        public int getItemCount() {
            return mHikes.size();
        }

        public void setHikes(List<Hike> hikes) {
            mHikes = hikes;
        }
    }
}
