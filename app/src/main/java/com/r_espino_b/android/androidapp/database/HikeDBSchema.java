package com.r_espino_b.android.androidapp.database;

/* HikeDBSchema: defines the blueprint and organization
*  for the actual database created in HikeOpenHelper.
*  Contains the definition for the single table within
*  the database, HikeTable.
*/
public class HikeDBSchema {
    /* HikeTable: contains the table's NAME and
    *  COLUMN (attributes) definitions that are
    *  used when implementing the database and
    *  its table in HikeOpenHelper.
    */
    public static final class HikeTable {
        //table name
        public static final String NAME = "hikes";

        //column (attribute) names
        public static final class COLUMNS {
            public static final String UUID = "uuid";
            public static final String DATE = "date";
            public static final String TITLE = "title";
            public static final String TIME = "time";
            public static final String DISTANCE = "distance";
            public static final String ALTITUDE = "altitude";
        }
    }
}