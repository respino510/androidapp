package com.r_espino_b.android.androidapp;

import java.util.Date;
import java.util.UUID;

public class Hike {
    private UUID mHikeID;           //used to uniquely identify each individual hike
    private Date mHikeDate;         //used to record the date of each individual hike
    private String mHikeName;       //used to name each hike based on its location
    private String mHikeTime;       //from chronometer- used to record hike duration
    private String mHikeDistance;   //used to record the total distance travelled
    private String mHikeAltitude;   //used to record the highest altitude during hike

    //used to keep track of the number of photos per individual hikes
    private static int imageCount = 0;

    public Hike() {
        this(UUID.randomUUID());
    }
    public Hike(UUID hikeID) {
        mHikeID = hikeID;
        mHikeDate = new Date();
    }

    public UUID getHikeID() {
        return mHikeID;
    }

    public void setHikeDate(Date hikeDate) {
        mHikeDate = hikeDate;
    }
    public Date getHikeDate() {
        return mHikeDate;
    }

    public void setHikeName(String hikeName) {
        mHikeName = hikeName;
    }
    public String getHikeName() {
        return mHikeName;
    }

    public void setHikeTime(String hikeTime) {
        mHikeTime = hikeTime;
    }
    public String getHikeTime() {
        return mHikeTime;
    }

    public void setHikeDistance(String hikeDistance) {
        mHikeDistance = hikeDistance;
    }
    public String getHikeDistance() {
        return mHikeDistance;
    }

    public void setHikeAltitude(String hikeAltitude) {
        mHikeAltitude = hikeAltitude;
    }
    public String getHikeAltitude() {
        return mHikeAltitude;
    }

    public String getPhotoFilename() {
        //imageCount++;
        return "IMG_" + getHikeID().toString() + "_" + imageCount /*+ ".jpg"*/;
    }
}
