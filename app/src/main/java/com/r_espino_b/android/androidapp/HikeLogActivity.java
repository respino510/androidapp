package com.r_espino_b.android.androidapp;

import android.support.v4.app.Fragment;

public class HikeLogActivity extends SingleFragmentActivity {

    @Override
    protected Fragment createFragment() {
        return new HikeLogFragment();
    }
}
