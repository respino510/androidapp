package com.r_espino_b.android.androidapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

/* MainActivity: this activity is the launcher activity (home screen)
*  that will host the functionality options for the app: begin a new
*  hike, plan a future hike, or view past hike data.
*/
public class MainActivity extends AppCompatActivity {
    private Button mBeginHikeBtn;
    private Button mPlanHikeBtn;
    private Button mHikeLogBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        /* mBeginHikeBtn: when clicked, the button initiates an intent to call
        *  HikeActivity, which hosts the HikeFragment.
        */
        mBeginHikeBtn = (Button)findViewById(R.id.begin_hike_btn);
        mBeginHikeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, HikeActivity.class);
                startActivity(intent);
            }
        });

        mPlanHikeBtn = (Button)findViewById(R.id.plan_hike_btn);
        mPlanHikeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(MainActivity.this, R.string.plan_hike_btn_toast, Toast.LENGTH_SHORT).show();
            }
        });

        mHikeLogBtn = (Button)findViewById(R.id.hike_log_btn);
        mHikeLogBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, HikeLogActivity.class);
                startActivity(intent);
            }
        });
    }
}
