package com.r_espino_b.android.androidapp.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.r_espino_b.android.androidapp.database.HikeDBSchema.HikeTable;

/* HikeOpenHelper: creates the database based on everything in
*  HikeDBSchema. Implements the methods from SQLiteOpenHelper
*  in order to create or allow access to the database if it has
*  already been created, and allows for updates to the database.
*/
public class HikeOpenHelper extends SQLiteOpenHelper {
    //update when changes to the database are made
    private static final int VERSION = 1;
    private static final String DB_NAME = "hikeBase.db";

    public HikeOpenHelper(Context context) {
        super(context, DB_NAME, null, VERSION);
    }

    //checks for existence of table, if nonexistent creates it, otherwise opens it
    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + HikeDBSchema.HikeTable.NAME + "(" +
                HikeTable.COLUMNS.UUID + ", " +
                HikeTable.COLUMNS.DATE + ", " +
                HikeTable.COLUMNS.TITLE + ", " +
                HikeTable.COLUMNS.TIME + ", " +
                HikeTable.COLUMNS.DISTANCE + ", " +
                HikeTable.COLUMNS.ALTITUDE + ")");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}