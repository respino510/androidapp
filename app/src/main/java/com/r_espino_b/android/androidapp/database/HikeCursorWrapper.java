package com.r_espino_b.android.androidapp.database;

import android.database.Cursor;
import android.database.CursorWrapper;

import com.r_espino_b.android.androidapp.Hike;
import com.r_espino_b.android.androidapp.database.HikeDBSchema.HikeTable;

import java.util.Date;
import java.util.UUID;

/* HikeCursorWrapper: used to query the database using a cursor,
*  but it extends the functionality of the cursor by creating the
*  getHike() method to extracting a row's (single Hike object) data
*  and setting it equal to a newly created Hike object that will be
*  returned.
*/
public class HikeCursorWrapper extends CursorWrapper {
    public HikeCursorWrapper(Cursor cursor) {
        super(cursor);
    }

    /* getHike() returns an individual hike from the database
    *  table by retrieving an individual row's (Hike object)
    *  column (attribute) values and setting them to a Hike
    *  object to return.
    */
    public Hike getHike() {
        //retrieves individual column (attribute) values
        String uuidString = getString(getColumnIndex(HikeTable.COLUMNS.UUID));
        long date = getLong(getColumnIndex(HikeTable.COLUMNS.DATE));
        String title = getString(getColumnIndex(HikeTable.COLUMNS.TITLE));
        String time = getString(getColumnIndex(HikeTable.COLUMNS.TIME));
        String distance = getString(getColumnIndex(HikeTable.COLUMNS.DISTANCE));
        String altitude = getString(getColumnIndex(HikeTable.COLUMNS.ALTITUDE));

        //sets retrieved column (attribute) values equal to a Hike object
        Hike hike = new Hike(UUID.fromString(uuidString));
        hike.setHikeDate (new Date(date));
        hike.setHikeName(title);
        hike.setHikeTime(time);
        hike.setHikeDistance(distance);
        hike.setHikeAltitude(altitude);

        //return initialized Hike object
        return hike;
    }
}