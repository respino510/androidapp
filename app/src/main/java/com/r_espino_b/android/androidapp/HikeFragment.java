package com.r_espino_b.android.androidapp;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.FileProvider;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.ImageButton;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;
import java.util.List;


public class HikeFragment extends Fragment {
    private static final int REQUEST_PHOTO = 1;

    private Hike mHike;

    private Button mEndHikeBtn;
    private Chronometer mChronometer;
    private ImageButton mCameraButton;
    private File mInPhotoFile;

    private String hikeTime;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mHike = new Hike();
        mInPhotoFile = HikeSingleton.get(getActivity()).getInPhotoFile(mHike);
        }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_hike, container, false);

        mChronometer = (Chronometer)v.findViewById(R.id.chronometer);
        startChronometer();

        PackageManager packageManager = getActivity().getPackageManager();
        mCameraButton = (ImageButton)v.findViewById(R.id.camera_imgbtn);
        final Intent takePhoto = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        boolean canTakePhoto = mInPhotoFile != null &&
                takePhoto.resolveActivity(packageManager) != null;

        mCameraButton.setEnabled(canTakePhoto);

        mCameraButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Uri inUri = FileProvider.getUriForFile(getActivity(),
                        "com.r_espino_b.android.androidapp.fileprovider",
                        mInPhotoFile);

                takePhoto.putExtra(MediaStore.EXTRA_OUTPUT, inUri);

                List<ResolveInfo> cameraActivites = getActivity()
                        .getPackageManager().queryIntentActivities(takePhoto,
                                PackageManager.MATCH_DEFAULT_ONLY);

                for(ResolveInfo activity : cameraActivites) {
                    getActivity().grantUriPermission(activity.activityInfo.packageName,
                            inUri, Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                }

                startActivityForResult(takePhoto, REQUEST_PHOTO);
            }
        });

        mEndHikeBtn = (Button)v.findViewById(R.id.end_hike_btn);
        mEndHikeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                stopChronometer();
                setHikeData();
                Toast.makeText(getActivity(), hikeTime, Toast.LENGTH_SHORT).show();
            }
        });

        return v;
    }

    public void startChronometer() {
        mChronometer.start();
    }
    public void stopChronometer() {
        mChronometer.stop();
    }

    //retrieves all hike data and saves it
    //TODO- add all hike data in it's own function
    public void setHikeData() {
        hikeTime = mChronometer.getText().toString();
        mHike.setHikeName("No name");
        mHike.setHikeTime(hikeTime);
        mHike.setHikeDistance("0 ft");
        mHike.setHikeAltitude("0 ft");

        HikeSingleton.get(getActivity()).addHike(mHike);
    }

    //persist all necessary data here
    @Override
    public void onPause() {
        super.onPause();

        /* assuming the user will not come back to the app we store
        *  the hike object and its data; currently the only way to
        *  return to the main activity is through using the back button.
        *  The functionality should be added to the mEndHikeBtn.
        *
        */
        /* TODO- currently if mEndHikeButton is pressed all data is persisted,
           but we don't return to the MainActivity. We return by pressing the back,
           which stores the same data again. Will fix by returning to MainActivity
           after pressing mEndHikeButton
        */
        setHikeData();
    }
}
