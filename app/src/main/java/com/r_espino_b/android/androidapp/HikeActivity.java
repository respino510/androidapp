package com.r_espino_b.android.androidapp;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;

import java.util.UUID;

public class HikeActivity extends SingleFragmentActivity {
    public static final String HIKE_ID = "com.r_espino_b.android.androidapp.hike_id";

    //allows us to pass Hike objects to other activities
    public static Intent newIntent(Context packageContext, UUID hikeID) {
        Intent intent = new Intent(packageContext, HikeActivity.class);
        intent.putExtra(HIKE_ID, hikeID);
        return intent;
    }

    /* This method call depends on SingleFragmentActivity,
    *  which allows HikeActivity to host HikeFragment and
    *  its view within its own view.
    */
    @Override
    protected Fragment createFragment() {
        return new HikeFragment();
    }
}
